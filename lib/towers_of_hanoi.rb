class TowersOfHanoi
  #6-7:15 for main. 7:30.... for working
  attr_reader :towers
  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def move(from_tower,to_tower)
    @from_tower = from_tower
    @to_tower = to_tower
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower,to_tower)
    return false if @towers[from_tower].empty?
    return true if @towers[to_tower].empty?
    return true if @towers[from_tower].pop < @towers[to_tower][-1]
  end

  def won?
    @towers[-1].length == 3 || @towers[-2].length == 3 ? true : false
  end

  def play
      puts "Welcome to the Towers of Hanoi!!"
      puts "Reminder: quit any time replying with q"
      puts "----------------------------------------"
    until won?
      @towers
      nums = ["0","1","2"]
      puts "Which peg do you select from? #{nums.join(" or ").strip}?"
      from = gets.chomp

      if from == "q"
        return "Thanks for trying I guess"
      end

      p "You chose [#{@towers[from.to_i].last}] from peg _#{from}_"

      other_nums = nums.reject {|n| n == from}

      puts "Now where would you like to put it? Peg #{other_nums.join(" or ").strip}?"
      to = gets.chomp

      if to == "q"
        return "Thanks for trying I guess"
      end

      p "You want to send [#{@towers[from.to_i].last}] to peg _#{to}_"

      if valid_move?(from.to_i,to.to_i)
        p "STARTING: #{@towers}"
        move(from.to_i,to.to_i)
        p "NEW STACK: #{@towers}"
      else
        p "This is an invalid move.  try again!"
      end
    end
    return "You are the god-king of Hanoi!"
  end

  def render
    puts @towers
  end
end
